import org.junit.jupiter.api.*;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation.class)
public class Calculation {

    private  int  result =6;
    @BeforeAll
    public static void sample(){
        new Thread(()->System.out.println("inside test with Lambda ")).start();
    }

    @Test
    @Order(1)
    public void addition(){
        Calculator calculator=new Calculator();
        Calculator.Calculate add = (arg1, arg2) -> arg1 + arg2;
         result = calculator.operate(3, result, add);
        Assertions.assertEquals(result,9);
    }

    @Test
    @Order(2)
    public void subtraction(){
        Calculator calculator=new Calculator();
        Calculator.Calculate sub = (arg1, arg2) -> arg1 - arg2;
         result = calculator.operate(10,result, sub);
        Assertions.assertEquals(4,result);
    }
}
