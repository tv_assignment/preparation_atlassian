import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GetAllUsersTests {
    private final String HOST = "localhost";
    private final int PORT = 8080;
    private WireMockServer server = new WireMockServer(PORT);

    public void responseBuilder(String END_POINT,String filePath){
        ResponseDefinitionBuilder mockResponse = new ResponseDefinitionBuilder();
        mockResponse.withStatus(200);
        mockResponse.withBodyFile(filePath);
        WireMock.stubFor(WireMock.get(END_POINT).willReturn(mockResponse));
    }

    @BeforeAll
    public void setup(){
        server.start();
        WireMock.configureFor(HOST,PORT);
    }

    @RepeatedTest(2)
    public void shouldGetAllStubUsers(){
        String END_POINT = "/public/v1/users";
        responseBuilder(END_POINT,"json/users.json");
        String api="http://localhost:"+PORT+END_POINT;
        Response response = given()
                .when()
                .get(api);
        response
                .then()
                .log().body();
        assertEquals(response.statusCode(),200);
    }

    @ParameterizedTest
    @CsvSource({
            "json/users.json, /public/v1/users",
            "json/user.json, /public/v2/users/1",
    })
    public void shouldGetAllUsers(String filepath,String END_POINT){
        responseBuilder(END_POINT,filepath);
        String api="http://localhost:"+PORT+END_POINT;
        Response response = given()
                .when()
                .get(api);
        response
                .then()
                .log().body();
        assertEquals(response.statusCode(),200);
    }
}

