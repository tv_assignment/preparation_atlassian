import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Sorting_integer {
    @Test
   public void sortUsingComparator(){
        ArrayList<Integer> integerArrayList= new ArrayList<Integer>();
        integerArrayList.add(15);
        integerArrayList.add(10);
        integerArrayList.add(0);
        integerArrayList.add(5);
        Comparator<Integer> integerComparator=(Int1,Int2) -> (Int1<Int2)?-1:(Int1>Int2)?1:0;
        Collections.sort(integerArrayList,integerComparator);
        System.out.println(integerArrayList);
        integerArrayList.stream().forEach(System.out::println);
        //even number
        List<Integer> evenInteger=integerArrayList.stream().filter(i->i%2==0).collect(Collectors.toList());
        System.out.println((evenInteger));
        //odd number
        List<Integer> oddInteger=integerArrayList.stream().filter(i->i%2!=0).collect(Collectors.toList());
        System.out.println((oddInteger));
    }
}
