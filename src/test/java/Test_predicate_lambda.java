import org.junit.jupiter.api.Assertions;
import java.util.List;
import java.util.function.Predicate;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;

public class Test_predicate_lambda {

    @ParameterizedTest
    @ValueSource(strings = {"J","j","v"})
    public void languagesStartsWithJ(String input){
        System.out.println("input"+ input);
        String[] languages = {"Java", "Scala", "C++", "Python", "Lisp"};
        Predicate_lambda predicateLambda=new Predicate_lambda();
        Predicate<String> predicate=str->str.startsWith(input);
        String[] out = predicateLambda.filter(List.of(languages), predicate).toArray(new String[0]);
        System.out.println(predicateLambda.filter(List.of(languages), predicate));
        Assertions.assertArrayEquals(new String[] {"Java"},out);

    }
    @ParameterizedTest
    @ValueSource(strings = {"J","a","v"})
    public void languagesEndsWithA(String input){
        String[] languages = {"Java", "Scala", "C++", "Python", "Lisp"};
        Predicate_lambda predicateLambda=new Predicate_lambda();
        Predicate<String> predicate=str->str.endsWith(input);
        String[] out = predicateLambda.filter(List.of(languages), predicate).toArray(new String[0]);
        System.out.println(predicateLambda.filter(List.of(languages), predicate));
        Assertions.assertArrayEquals(new String[] {"Java","Scala"},out);

    }

}
