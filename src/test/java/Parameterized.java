import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;

import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

public class Parameterized {
    @ParameterizedTest
    @EnumSource(value = Month.class, names = {"APRIL", "JUNE", "SEPTEMBER", "JULY"})
    void someMonths_Are30DaysLong(Month month) {
        final boolean isALeapYear = false;
        assertEquals(30, month.length(isALeapYear));
    }
}
