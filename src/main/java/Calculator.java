public class Calculator {
    @FunctionalInterface
    public interface Calculate{
        int operation(int arg1,int arg2);
    }
    public int operate(int arg1,int arg2,Calculate cal){
        return cal.operation(arg1,arg2);
    }
}
