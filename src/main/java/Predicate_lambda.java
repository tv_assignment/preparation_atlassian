import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Predicate_lambda {

    public ArrayList<String> filter(@NotNull List names, Predicate condition){
        ArrayList <String> nameList=new ArrayList<>();
        names.stream().filter((name)->(condition.test(name)))
                .forEach((name)->{
                    nameList.add((String) name);
                });
         return nameList;
    }
}
